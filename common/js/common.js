let screenHeight = "";
let wHeight = "";
let wWidth = $(window).width();
let height=window.innerHeight;
let globalNav__nav = "#globalNav__nav"
let v = document.getElementById('v');

var tl = new TimelineMax({
    paused : true
});

let btn = document.querySelectorAll('.arrowBtn01');
let btElm = document.querySelectorAll('.arrowBtn01__underBorder span');




// このファイル読み込み時に即実行されるコード
v.pause();

window.onload = function() {
    // const spinner = document.getElementById('loading');
    // spinner.classList.add('loaded');
    vOn();
}

$(function(){
    $('a[href^="#"]').click(function(){
        let speed = 500;
        let href= $(this).attr("href");
        let target = $(href == "#" || href == "" ? 'html' : href);
        let position = target.offset().top;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });
});

// 関数
let vOn = function(){
    v.muted = true;
    v.play();
}
// loading + animation
// window.onload = function() {
//     const spinner = document.getElementById('loading');
//     spinner.classList.add('loaded');
//     callback();animationStart();
// }
// function animationStart(){
// 	scrollrr = $(this).scrollTop();
// 	screenHeight = $(window).innerHeight()+scrollrr;
// 	callback();
// };
// function callback(){    
// 	$('.js_animate').each(function() {
// 		itemHeight = $(this).offset().top;
// 		if( screenHeight > itemHeight ){
// 			$(this).addClass('js_animationOn')
// 		}
// 	});
// }
// // スクロールで制御
// window.addEventListener('scroll', throttle(callback, 50));
// function throttle(fn, wait) {
// 	let time = Date.now();
// 	return function() {
// 		screenHeight = $(this).scrollTop()+wHeight;
//         if ((time + wait - Date.now()) < 0) {
//             fn();
//             time = Date.now();
//             wHeight = $(window).height();
//         }
// 	}
// }

// イベントハンドラ
$(function(){
    $(document).on('click', '#indexBtn', function () { 
        $('#globalNav__circle').toggleClass('js_active');
    });
});

window.addEventListener('resize', () => {
    height=window.innerHeight;
    document.documentElement.style.setProperty( '--myheight', height/100 + 'px');
});
document.documentElement.style.setProperty( '--myheight', height/100 + 'px');




let btnAnimation = TweenMax
    .to(btElm, .5, { 
        paused: true,
        width: "100%",
        x:'0%',
        ease: "power2.out",
    });

let btnAnimation2 = tl
    .to(btElm,.5,{
        pause: true,
        x: '100%',
        width: "100%",
        ease: "power2.out",
    })
    .to(btElm,0,{
        x:'0%',
        width:'0%',
        ease: "power2.out",
    })
    .to(btElm,.2,{
        x:'0%',
        width:'46px',
        ease: "power2.out",
    })

btn.addEventListener("mouseenter", () => btnAnimation.play(0));
btn.addEventListener("mouseleave", () => btnAnimation2.play(0));
